import { serve } from "@hono/node-server";
import { Hono } from "hono";
import { Collection, MongoClient } from "mongodb";

const booksData = [
  {
    title: "Lord of the Rings",
    genre: "Fantasy",
  },
  {
    title: "Dune",
    genre: "Science Fiction",
  },
  {
    title: "To Kill a Mockingbird",
    genre: "Fiction",
  },
  { title: "Romeo and Juliet", genre: "tragedy" },
];

let booksCollection: Collection;
const connectToDb = async () => {
  const url = process.env.MONGO_URL || "mongodb://localhost:27017";
  const client = new MongoClient(url);
  await client.connect();

  booksCollection = client.db("books").collection("books");
  booksCollection.deleteMany();
  booksCollection.insertMany(booksData);
};
connectToDb();

const app = new Hono();

const port = 5000;
console.log(`Server is running on port ${port}`);

serve({
  fetch: app.fetch,
  port,
});

app.get("/books", async (c) => {
  const books = await booksCollection.find().toArray();
  return c.json(books);
});
