interface Book {
  title: string;
  genre: string;
}

const RootPage = async () => {
  const response = await fetch("http://backend:5000/books", {
    cache: "no-store",
  });
  const books: Book[] = await response.json();

  return (
    <div>
      {books && (
        <>
          <h1>Books</h1>
          <ul>
            {books.map((book) => (
              <li key={book.title}>
                {book.title}, genre: {book.genre}
              </li>
            ))}
          </ul>
        </>
      )}
    </div>
  );
};

export default RootPage;
